// Importables
import {useState} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import AboutUs from './components/AboutUs.js'
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Orders from './pages/Orders.js';
import NotFound from './pages/NotFound.js';
import Admin from './pages/Admin.js';
import CreateProduct from './pages/CreateProduct.js'
import ProductView from './components/ProductView.js';
import {Container} from 'react-bootstrap';
import './App.css';
import { UserProvider } from './UserContext.js';
import 'bootstrap/dist/css/bootstrap.min.css'

// Component function
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null 
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container fluid={true} className="p-0">
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products/active" element={<Products/>}/>
            <Route path="/products/:productId/view" element={<ProductView/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/admin" element={<Admin/>}/>
            <Route path="/location" element={<AboutUs/>}/>
            <Route path="/products/create" element={<CreateProduct/>}/>
            <Route path="/users/allusers" element={<Orders/>}/>
            <Route path="*" element={<NotFound/>}/> 

          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

// Exporting of the component function
export default App;
