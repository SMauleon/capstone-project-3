import {useState, useEffect, Fragment } from 'react';
import ProductCard from '../components/ProductCard';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col, Container } from 'react-bootstrap';
import Footer from '../components/Footer';

export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(result => {
			setIsLoading(false)

			setProducts(result.activeProducts.map(product => {
				return (
					<ProductCard key={product._id} product={product}/>
				)
			}))
		})
	}, [])

	return(
		<Fragment>
  <Container className="my-5 d-flex flex-column align-items-center">
    <h1>Products</h1>
    <Row>
      <Col className="d-flex flex-wrap justify-content-center">
        { isLoading ?
          <LoadingSpinner/>
          :
          products 
        }
      </Col>
    </Row>
  </Container>
  <Footer />
</Fragment>

	)
	
}
