import {Container} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom'
import { Link } from 'react-router-dom';

export default function NotFound() {
	const navigate = useNavigate();
	const goBack = () => {
		navigate(-1);
	};

	return (
		<Container fluid className="not_found" style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
		<img height="600px" src="https://img.freepik.com/free-vector/oops-404-error-with-broken-robot-concept-illustration_114360-1932.jpg?w=2000" alt="404 Not Found"/>
		<h1>Page Not Found</h1>
		<p>Go back to the <Link href="#" to={goBack}>previous page</Link>.</p>
		</Container>
	)
}