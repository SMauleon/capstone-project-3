import { useState, useEffect, useContext, Fragment } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import Footer from '../components/Footer'

export default function Login(){
  const { user, setUser } = useContext(UserContext)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isActive, setIsActive] = useState(false)


  function loginUser(event){
    event.preventDefault()

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(result => {
      if(typeof result.accessToken !== 'undefined'){
        localStorage.setItem('token', result.accessToken)
        retrieveUserDetails(result.accessToken)

        // Clear Form
        setEmail('')
        setPassword('')

        Swal.fire({
          title: "Login Successful",
          icon: "success",
          text: "Welcome to Zovelo Jewelry Shop"
        })
      } 
    }).catch(error => {
      Swal.fire({
        title: "Authentication failed",
        icon: "error",
        text: "Kindly check your login credentials."
      })
    })

    
  }

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:3001/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(response => response.json())
    .then(result => {
      setUser({
        id: result._id,
        isAdmin: result.isAdmin
      })
    })
  }

  useEffect(() => {
    if(email !== '' && password !== ''){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])


  return(
    (user.id !== null) ?
      (user.isAdmin ? <Navigate to="/admin" /> : <Navigate to="/" />)
    : 
    <Fragment>
    <Row className="justify-content-center" style={{ margin: '30px', marginTop: '50px' }}>
  <Col xs={12} md={4} style={{ backgroundColor: '#e0cdad', borderRadius: '10px' }}>
    <Form onSubmit={(event) => loginUser(event)} style={{ padding: '20px' }}>
      <h1>Login</h1>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter email" 
          value={email}
          onChange={event => setEmail(event.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Password"
          value={password}
          onChange={event => setPassword(event.target.value)} 
          required
        />
      </Form.Group>

      { isActive ?
        <Button className="my-3" style={{ backgroundColor: "#3b271d", color: "#ffffff" }} type="submit" id="submitBtn">
          Submit
        </Button>
        :
        <Button className="my-3" disabled style={{ backgroundColor: "#3b271d", color: "#ffffff" }} type="submit" id="submitBtn">
          Submit
        </Button>
      }
    </Form>
  </Col>
</Row>
<Footer />
</Fragment>




  )
}
