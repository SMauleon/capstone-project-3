import { useState, useEffect, useContext, Fragment } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Footer from '../components/Footer';

export default function Register(){
  const navigate = useNavigate()

  const { user } = useContext(UserContext)

  const [nickName, setNickName] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobileNumber, setMobileNumber] = useState('')
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [isActive, setIsActive] = useState(false)


  function registerUser(event){
    event.preventDefault()

    fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email 
      })
    })
    .then(response => response.json())
    .then(result => {
      if(result === true){
        Swal.fire({
          title: "User Already exists!",
          icon: "error",
          text: "The email you provided already exists in the server! Provide another email for registration."
        })
      } else {
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            nickname: nickName,
            email: email,
            mobileNo: mobileNumber,
            password: password1
          })
        })
        .then(response => response.json())
        .then(result => {
          if(typeof result !== "undefined"){
            // Clear form
            setFirstName('')
            setLastName('')
            setMobileNumber('')
            setEmail('')
            setPassword1('')
            setPassword2('')

            Swal.fire({
              title: "Registration Successful!",
              icon: "success",
              text: "Welcome to Zovelo Jewelry Shop"
            })

            navigate("/login")
          }
        }).catch(error => {
          Swal.fire({
            title: "Something went wrong :(",
            icon: "error",
            text: error.message
          })
        })
      }
    })
  }

  useEffect(() => {
    if((firstName !== '' && lastName !== '' && mobileNumber !== '' && email !== '' && password1 !== '' && password2 !== '') && (nickName !== '') &&(password1 === password2)){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [firstName, lastName, mobileNumber, email, nickName, password1, password2])

  return(
    (user.id !== null) ?
      <Navigate to="/courses"/>
    : 
    <Fragment>
    <Row className="justify-content-center" style={{ margin: '30px', marginTop: '50px'}}>
      <Col xs={12} md={4} style={{ backgroundColor: '#e0cdad', borderRadius: '10px' }}>
        <Form onSubmit={(event) => registerUser(event)} style={{ padding: '20px' }}>
          <h1>Register</h1>
          <Form.Group controlId="userFirstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control 
                      type="text" 
                      placeholder="Enter your First Name" 
                      value={firstName}
                      onChange={event => setFirstName(event.target.value)}
                      required
                  />
              </Form.Group>

              <Form.Group controlId="userLastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control 
                      type="text" 
                      placeholder="Enter your Last Name" 
                      value={lastName}
                      onChange={event => setLastName(event.target.value)}
                      required
                  />
              </Form.Group>

                  <Form.Group controlId="userNickName">
                  <Form.Label>Nickname</Form.Label>
                  <Form.Control 
                      type="text" 
                      placeholder="Enter your Nickname" 
                      value={nickName}
                      onChange={event => setNickName(event.target.value)}
                      required
                  />
              </Form.Group>

              <Form.Group controlId="userMobileNumber">
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control 
                      type="text" 
                      placeholder="Enter your Mobile Number" 
                      value={mobileNumber}
                      onChange={event => setMobileNumber(event.target.value)}
                      required
                  />
              </Form.Group>

              <Form.Group controlId="userEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control 
                      type="email" 
                      placeholder="Enter email" 
                      value={email}
                      onChange={event => setEmail(event.target.value)}
                      required
                  />
                  <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                  </Form.Text>
              </Form.Group>

              <Form.Group controlId="password1">
                  <Form.Label>Password</Form.Label>
                  <Form.Control 
                      type="password" 
                      placeholder="Password"
                      value={password1}
                      onChange={event => setPassword1(event.target.value)} 
                      required
                  />
              </Form.Group>

              <Form.Group controlId="password2">
                  <Form.Label>Verify Password</Form.Label>
                  <Form.Control 
                      type="password" 
                      placeholder="Verify Password" 
                      value={password2}
                      onChange={event => setPassword2(event.target.value)}
                      required
                  />
              </Form.Group>

              { isActive ?
                <Button className="my-3" style={{ backgroundColor: "#3b271d", color: "#ffffff" }} type="submit" id="submitBtn">
                  Submit
                </Button>
                :
                <Button className="my-3" disabled style={{ backgroundColor: "#3b271d", color: "#ffffff" }} type="submit" id="submitBtn">
                  Submit
                </Button>
              }
          </Form>
      </Col>
    </Row>
    <Footer />
    </Fragment>
  )
}
