import { Fragment } from 'react';
import Banner from '../components/Banner';
import Footer from '../components/Footer';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Home(){
	return (
		<>
		<Fragment>
      <Banner />
      <Container>
        <Row>
          <Col className="my-5 d-flex justify-content-center align-items-center">
            <div className="about-content">
              <h2>About Us</h2>
              <p>
                Welcome to Zovelo! This is a startup jewelry shop, where we specialize in stainless steel jewelry that combines durability, style, and affordability. Our passion for providing high-quality accessories has driven us to curate a diverse collection that caters to the modern and fashion-forward individuals. At our jewelry shop, we take pride in offering a wide range of stainless steel jewelry pieces that are meticulously crafted to elevate your style and stand the test of time. Stainless steel jewelry is known for its exceptional strength, resistance to tarnish, and hypoallergenic properties, making it an excellent choice for those with sensitive skin.
              </p>
			  <Link className="btn" to="/location" style={{ backgroundColor: "#3b271d", color: "#ffffff" }}>Learn More</Link>
            </div>
          </Col>
        </Row>
      </Container>
	  		<div style={{ display: "flex", justifyContent: "center", margin: "0 auto" }}>
				<img width="80%" src="pack.jpg" alt="Packaging Pictures"></img>
			</div>
			<div style={{ display: "flex", justifyContent: "center", margin: "0 auto" }}>
				<img width="80%" src="neck.jpg" alt="Necklace Pictures"></img>
			</div>
			<div className="button-container">
			<Link className="btn" to="/products/active" style={{ backgroundColor: "#3b271d", color: "#ffffff" }}>Explore More Products</Link>
			</div>
      <Footer />
    </Fragment>
		</>
	)
}
