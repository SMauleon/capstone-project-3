import {useState, useEffect} from 'react'
import LoadingSpinner from '../components/LoadingSpinner'
import { Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import AdminProductCard from '../components/AdminProductCard'

export default function AdminDashBoard(){
  const [products, setProducts] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    setIsLoading(true)

    fetch(`${process.env.REACT_APP_API_URL}/products`)
    .then(response => response.json())
    .then(result => {

      setIsLoading(false)

      setProducts(result.products.map(product => {
        return (
          <AdminProductCard key={product._id} product={product}/>
        )
      }))
    })
  }, [])

  return(
    <>
    <div style={{ backgroundColor: "#e0cdad", textAlign: "center", padding: "20px" }}>
  <h1>Admin Dashboard</h1>
  <h5>Create a new product</h5>
  <Link style={{ backgroundColor: "#3b271d", color: "#ffffff" }} className="btn mx-2" to="/products/create">Create a Product</Link>
  <Row>
    <Col>
      { isLoading ?
          <LoadingSpinner/>
        :
          products 
      }
    </Col>
  </Row>
</div>

    </>
  )
}
