// To use react bootstrap components, you must first import them from the react-bootstrap package
import { Fragment, useContext } from 'react';
import {Container, Navbar, Nav} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){
  // Gets the user email after the user has logged in
  const { user } = useContext(UserContext)

  return(
    <>
      <Navbar>
        <Container className="navbar font-link">
          <Nav className="me-auto">
            <Nav.Link style={{ color: 'white' }} as= {NavLink} to= "/">Home</Nav.Link>
            <Nav.Link style={{ color: 'white' }} as= {NavLink} to= "/products/active">Menu</Nav.Link>
             
             { (user.isAdmin) ?
              <Nav.Link style={{ color: 'white' }} as= {NavLink} to="/admin">Admin Dashboard</Nav.Link>
              :
              <Nav.Link style={{ color: 'white' }} as= {NavLink} to="/location">About Us</Nav.Link>
            }
           {  (user.id !== null) ?
             <Nav.Link style={{ color: 'white' }} as= {NavLink} to="/logout">Logout</Nav.Link>
             :
             <Fragment>
            <Nav.Link style={{ color: 'white' }} as= {NavLink} to="/login">Log In</Nav.Link>
            <Nav.Link style={{ color: 'white' }} as= {NavLink} to="/register">Register</Nav.Link>
            </Fragment>
           }

          </Nav>
        </Container>
      </Navbar>
    </>
  )
}
