import { Fragment } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import Footer from '../components/Footer';

function AboutUs() {
  return (
    <Fragment>
    <Container>
      <Row>
        <Col className="my-5 d-flex justify-content-center align-items-center">
          <div className="about-content">
            <h2 className="mb-3">About Us</h2>
            <p>
              Welcome to Zovelo! This is a startup jewelry shop, where we specialize in stainless steel jewelry that combines durability, style, and affordability. Our passion for providing high-quality accessories has driven us to curate a diverse collection that caters to the modern and fashion-forward individuals. At our jewelry shop, we take pride in offering a wide range of stainless steel jewelry pieces that are meticulously crafted to elevate your style and stand the test of time. Stainless steel jewelry is known for its exceptional strength, resistance to tarnish, and hypoallergenic properties, making it an excellent choice for those with sensitive skin.
            </p>
            <p>
              As an online store, you can find our curated selection of stainless steel jewelry not only on our website but also on popular online marketplaces like Shopee and Lazada. We strive to provide a seamless shopping experience, ensuring that you can explore and purchase your favorite pieces with ease and confidence. At our startup jewelry shop, we value customer satisfaction above all else. We are committed to providing exceptional service, prompt shipping, and secure packaging to ensure your jewelry arrives in pristine condition. Our dedicated customer support team is always ready to assist you with any inquiries, product recommendations, or concerns you may have.
            </p>
          </div>
          <div className="mt-3" style={{ display: "flex", justifyContent: "center", marginLeft: "50px"}}>
				    <img width="100%" src="about2.png" alt="Earring Pictures"></img>
			    </div>
        </Col>
      </Row>
    </Container>
          <div style={{ display: "flex", justifyContent: "center", margin: "0 auto" }}>
				    <img width="68%" src="about1.jpg" alt="Necklace Pictures"></img>
			    </div>
          <div className="my-5" style={{ textAlign: "justify", marginLeft: "16%", marginRight: "16%" }}>
        <p>
          Thank you for choosing our startup jewelry shop as your go-to destination for stylish and durable stainless steel jewelry. We invite you to explore our online store, browse through our collections, and find the perfect pieces that resonate with your personal style. Embark on a journey of self-expression and adorn yourself with our exceptional stainless steel jewelry today.
        </p>
      </div>
    <Footer />
    </Fragment>
  );
}

export default AboutUs;
