import { useState } from 'react';
import {Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UpdateProductModal from '../components/UpdateProductModal';
import Swal from 'sweetalert2' 



export default function AdminProductCard({product}){
  // You can destructure props twice to be able to access their properties directly without using dot (.) notation in the JSX code.
  const {_id, name, description, price, isActive} = product
  const [isArchived, setIsArchived] = useState(!isActive);
  const [isUnarchived, setIsUnarchived] = useState(isActive);
  const [showModal, setShowModal] = useState(false);

  const openModal = () => {
  setShowModal(true);
  };

  const closeModal = () => {
  setShowModal(false);
  };

  const handleModalSubmit = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/update`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
    .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          Swal.fire({
          title: "Oopsie daisy",
          icon: "error",
          text: "Something went wrong :("
        })
        }
          Swal.fire({
          title: "Success",
          icon: "success",
          text: result.message
        })
        closeModal();  
      }) 
  };

  const archiveProduct = () => {
      
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
    .then((response) => response.json())
      .then((data) => {
        if (data.message === 'Archived Product') {
          setIsArchived(true);
          setIsUnarchived(false);
          console.log('Product Archived', data);
        }
      })
      .catch((error) => {
        console.error('Error archiving product:', error);
      });
  };

const activateProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message === 'Activated Product') {
          setIsUnarchived(true);
          setIsArchived(false);
          console.log('Product activated:', data);
        }
      })
      .catch((error) => {
        console.error('Error activating product:', error);
      });
  };


  return(
    <>
    <div className="container">
  <div className="row">
    <div className="col-md-12">
      <Card className="my-3 mx-auto" style={{ maxWidth: "1000px" }}>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {price}.00</Card.Text>
          <div>
            <Link
              style={{ backgroundColor: "#3b271d", color: "#ffffff" }}
              className="btn mx-2"
              onClick={openModal}
            >
              Update
            </Link>
            {isArchived ? (
              <Link className="btn btn-danger" onClick={activateProduct}>
                Activate Product
              </Link>
            ) : (
              <Link
                style={{ backgroundColor: "#e0cdad", color: "black" }}
                className="btn"
                onClick={archiveProduct}
              >
                Archive Product
              </Link>
            )}
          </div>
        </Card.Body>
      </Card>
    </div>
    <div>
      <UpdateProductModal
        isOpen={showModal}
        closeModal={closeModal}
        product={product}
        handleSubmit={handleModalSubmit}
      />
    </div>
  </div>
</div>

    </>
  )
}

