import {Card} from 'react-bootstrap';



export default function OrderCard({user}){
  // You can destructure props twice to be able to access their properties directly without using dot (.) notation in the JSX code.
  const {_id, orders, nickname} = user


  return(
    <>
    <Card className="my-3"> 
      <Card.Body>
        <Card.Title>{_id}</Card.Title>

        <Card.Text>{nickname}</Card.Text>

        <Card.Text>{orders}</Card.Text>

      </Card.Body>
    </Card>
    </>
  )
}

