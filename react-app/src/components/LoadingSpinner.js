import {Spinner} from 'react-bootstrap'

export default function LoadingSpinner(){
	return(
		<Spinner className="spinner" animation="border" />
	)
}
